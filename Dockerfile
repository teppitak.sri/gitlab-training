# Use an existing docker image as a base
FROM ubuntu:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the current directory (where the Dockerfile is located) into the container at /app
COPY . /app

# Install any needed dependencies
RUN apt-get update && apt-get install -y \
    curl \
    netcat \
    && rm -rf /var/lib/apt/lists/*

# Specify a command to run on container start
CMD ["sleep 5000"]
