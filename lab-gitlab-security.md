# Security Configure in gitlab-ci.yml

## Content

- Static Application Security Testing (SAST)
- Secret Detection
- Dynamic Application Security Testing (DAST)
- API fuzzing
- Dependency Scanning
- Container Scanning
- Infrastructure as Code (IaC) Scanning

## ทุก Security สามารถเพิ่มค่าแต่ scanner ลงในไฟล์ `gitlab-ci.yml` 

### เพิ่ม Stages ที่จำเป็นสำหรับ Security
```
stages:
- code_scan
- image_scan
- dast
- fuzz
```

## Static Application Security Testing (SAST)

### Example

``` yaml
variables:
  SAST_DISABLED: "true" 

include:
- template: Security/SAST.gitlab-ci.yml

nodejs-scan-sast:
  stage: code_scan
  tags:
  - tkg-gitlab-runner-docker
  script:
    - /analyzer run
  artifacts:
    reports:
      sast: gl-sast-report.json
```
## Secret Detection

### Example

``` yaml
include:
- template: Jobs/Secret-Detection.gitlab-ci.yml

secret_detection:
  stage: code_scan
  tags:
  - tkg-gitlab-runner-docker
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: 'true'
```

## Dynamic Application Security Testing (DAST)

### กำหนดค่าการ DAST scan ภายใต้ Project ที่หัวข้อ `Secure > Security Configuration > Configure DAST`

### Example

``` yaml
- template: DAST.gitlab-ci.yml

dast:
  stage: dast
  tags:
  - tkg-gitlab-runner-docker
  dast_configuration:
    site_profile: "testfire"
    scanner_profile: "DAST"
```
## API fuzzing
การทำ API fuzzing จำเป็นต้องมีไฟล์ json collection ในการใช้งานให้เหมาะสมกับ API website ปลายทาง
และกำหนดการตั้งค่าที่ `Secure > Security Configuration > Configure API Fuzzing`

### Example
``` yaml
- template: API-Fuzzing.gitlab-ci.yml

apifuzzer_fuzz:
  stage: fuzz
  tags:
  - tkg-gitlab-runner-docker
  variables:
    FUZZAPI_POSTMAN_COLLECTION: postman_collection.v2.1.json
    FUZZAPI_TARGET_URL: http://149.248.44.52:7777
    FUZZAPI_PROFILE: Quick-10
```

## Dependency Scanning
### Example

```yaml
- template: Security/Dependency-Scanning.gitlab-ci.yml


gemnasium-dependency_scanning:
  stage: code_scan
  tags:
  - tkg-gitlab-runner-docker
```

## Container Scanning
### Example

```yaml
- template: Security/Container-Scanning.gitlab-ci.yml

container_scanning:
  stage: image_scan
  tags:
  - tkg-gitlab-runner-docker
  variables:
    CS_ANALYZER_IMAGE: registry.gitlab.com/security-products/container-scanning:6-fips
    GIT_STRATEGY: fetch
    SECURE_LOG_LEVEL: 'debug'
```


## Infrastructure as Code (IaC) Scanning

### Example
```yaml
- template: Security/SAST-IaC.gitlab-ci.yml

semgrep-sast:
  stage: code_scan
  needs: []
  tags:
  - tkg-gitlab-runner-docker

kics-iac-sast:
  stage: code_scan
  tags:
  - tkg-gitlab-runner-docker


```